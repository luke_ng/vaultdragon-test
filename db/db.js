/*
This module provides helper methods to allow the application 
to interact with a MongoDB database.

*/

var MongoClient = require('mongodb').MongoClient;


function DB() {
	this.db = null;			// The MongoDB database connection
}

DB.prototype.connect = function(uri) {

	// Connect to the database specified by the connect string / uri
	
	// Trick to cope with the fact that "this" will refer to a different
	// object once in the promise's function.
	var _this = this;
	
	// This method returns a javascript promise (rather than having the caller
	// supply a callback function).

	return new Promise(function(resolve, reject) {
		if (_this.db) {
			// Already connected
			resolve();
		} else {
			var __this = _this;
			
			// Many methods in the MongoDB driver will return a promise
			// if the caller doesn't pass a callback function.
			MongoClient.connect(uri)
			.then(
				function(database) {
					
					// The first function provided as a parameter to "then"
					// is called if the promise is resolved successfuly. The 
					// "connect" method returns the new database connection
					// which the code in this function sees as the "database"
					// parameter

					// Store the database connection as part of the DB object so
					// that it can be used by subsequent method calls.

					__this.db = database;

					// Indicate to the caller that the request was completed succesfully,
					// No parameters are passed back.

					resolve();
				},
				function(err) {

					// The second function provided as a parameter to "then"
					// is called if the promise is rejected. "err" is set to 
					// to the error passed by the "connect" method.

					console.log("Error connecting: " + err.message);

					// Indicate to the caller that the request failed and pass back
					// the error that was returned from "connect"

					reject(err.message);
				}
			)
		}
	})
}

DB.prototype.close = function() {
	
	// Close the database connection. This if the connection isn't open
	// then just ignore, if closing a connection fails then log the fact
	// but then move on. This method returns nothing – the caller can fire
	// and forget.

	if (this.db) {
		this.db.close()
		.then(
			function() {},
			function(error) {
				console.log("Failed to close the database: " + error.message)
			}
		)	
	}
}


DB.prototype.insertObject = function(coll, key, value) {

	var _this = this;

	return new Promise(function (resolve, reject) {
		
		_this.db.collection(coll, function(error, collection){
			if (error) {
				console.log("Could not access collection: " + error.message);
				reject(error.message);
			} else {
				var unixtime = new Date();
				//Truncating the milliseconds component
				unixtime.setMilliseconds(0);

				var newobj = {
					key: key,
					value: value,
					timestamp: unixtime
				};

				collection.insertOne(newobj)
				.then(
					function(result) {
						// Resolve the promise
						unixsecs = unixtime.getTime()/1000;
						newobj['unix_time'] = unixsecs;
						resolve(newobj);
					},
					function(err) {
						console.log("insertObject failed: " + err.message);
						// Reject the promise with the error passed back by the count
						// function
						reject(err.message);
					}
				)
			}
		});
	})
}


DB.prototype.getLatestValue = function(coll, mykey) {
	
	// Return a promise that resolves to the
	// most recent value 

	var _this=this;

	return new Promise(function (resolve, reject) {
		_this.db.collection(coll, {strict:false}, function(error, collection){
			if (error) {
				console.log("Could not access collection: " + error.message);
				reject(error.message);
			} else {
				var cursor = collection.find({key: mykey}).sort({timestamp: -1}).limit(1);
				cursor.toArray(function(error, docArray) {
			    	if (error) {
						console.log("Error reading from cursor: " + error.message);
						reject(error.message);
					} else {
						if(docArray && docArray[0]){
							resolve(docArray[0].value);
						}
						else{
							reject("Key does not exist");
						}
					}
		    	})
			}
		})
	})
}


DB.prototype.getValueByTime = function(coll, mykey, myDate) {
	
	// Return a promise that resolves to
	// whatever the value of the key at the time was.

	var _this=this;

	return new Promise(function (resolve, reject) {
		_this.db.collection(coll, {strict:false}, function(error, collection){
			if (error) {
				console.log("Could not access collection: " + error.message);
				reject(error.message);
			} else {
				myDate.setSeconds(myDate.getSeconds() + 1);

				var cursor = collection.find({ key: mykey, timestamp: {$lt: myDate} })
					.sort({timestamp: -1}).limit(1);
				cursor.toArray(function(error, docArray) {
			    	if (error) {
						console.log("Error reading fron cursor: " + error.message);
						reject(error.message);
					} else {
						if(docArray && docArray[0]){
							resolve(docArray[0].value);
						}
						else{
							reject(mykey + " does not exist OR it had not been created at this time");
						}
					}
		    	})
			}
		})
	})
}




// Make the module available for use in other files
module.exports = DB;