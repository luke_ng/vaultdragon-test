// Schema for "objects"

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var objectSchema = new Schema({
  key : { type: String, required: true },
  value : { type: String, required: true },
  timestamp : Date
});


var Obj = mongoose.model('Obj', objectSchema);

// make this available to our Node applications
module.exports = Obj;