# Version Controlled Key Value Store #

Build a version controlled key-value store with a HTTP API we can query that from. The API needs to be able to:

1. Accept a key(string) and value(some json blob/string) {"key" : "value"} and store them. If an existing key is sent, the value should be updated
1. Accept a key and return the corresponding latest value
1. When given a key AND a timestamp, return whatever the value of the key at the time was.


## Database Viewer ##

The database contents can be viewed graphically with the installed mongo-express. It's listening to port 3000:

[http://vaultdragon.hopto.org:3000](http://vaultdragon.hopto.org:3000)

Login with username "admin" and password "bigsecret"


## REST API ##

* * *

**Title :** Add a new "Object"

**URL :** /api/v1/object

**Method :** POST

**POST Body JSON :** { "key" : "value" }

**Response Codes :** Success (200 OK), Bad Request (400)

**Example responses**

```
#!json
(200 OK)
{
  "success": true,
  "error": "",
  "key": "mykey2",
  "value": "value653",
  "unix_time": 1496060549
}
```

```
Bad Request (400): 
POST parameter(s) invalid or missing
```

* * *


**Title :** Retrieve the value of an "Object"

**URL :** /api/v1/object/:mykey?timestamp=:mytime

**Method :** GET

**URL Params :** Required: mykey=[string] Optional: mytime=[UNIX timestamp]

**Response Codes:** Success (200 OK)

**Example responses**

```
#!json
(200 OK)
{
  "success": true,
  "value": "value111",
  "error": ""
}
```

```
#!json
(200 OK)
{
  "success": false,
  "value": "",
  "error": "Failed to retrieve value: Key does not exist"
}
```


## Some notes ##

Timestamps recorded in the DB are up to the millisecond (JS standard). Since the query resolution is in seconds (Unix standard), the timestamp's millisecond component is assumed to be 0.