/*
	Build the DB connection string from the config.js
*/
var config = require('../config.js');

function getDbUri(backendName){
	var backinfo = config.client[backendName];
	if(backinfo){
		var connStr = backinfo.defaultUri + "/" + backinfo.defaultDatabase;
		if(backinfo.opts){
			connStr += "?" + backinfo.opts;
		}
		return connStr;
	}

	return;
}

// Make the module available for use in other files
module.exports = getDbUri;