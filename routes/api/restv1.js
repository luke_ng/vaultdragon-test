var express = require('express');
var router = express.Router();

var config = require('../../config.js');
var DB = require('../../db/db.js');
//Build connection string of the MongoDB from config.js
var mongoDbUri = require('../../helper/getDbUri.js')('mongodb');


/* GET users listing. */
router.get('/', function(req, res, next) {
	res.send('Hello REST API V1');
});


// Creates a new object in DB
router.post('/object', (req, res, next) => {
	var inkeys = Object.keys(req.body);

	//Validate Inputs
	if(inkeys.length > 0){
		var newkey = inkeys[0].trim();
		var database = new DB;
		database.connect(mongoDbUri)
		.then(
			function() {
				return database.insertObject(config.client.mongodb.defaultCollection,
					newkey, req.body[inkeys[0]]);
			})
		.then(
			function(resObj) {
				var retObj = {
					"success": true,
					"error": ""
				};
				retObj['key'] = resObj.key;
				retObj['value'] = resObj.value;
				retObj['unix_time'] = resObj.unix_time;
				
				return retObj;
			},
			function(err) {
				console.log("Failed to insert Object: " + err);
				return {
						"success": false,
						"error": "Failed to insert Object: " + err
					};
			})
		.then(
			function(resultObject) {
				database.close();
				res.json(resultObject);
			})
	}
	else {
		res.status(400).send("POST parameter(s) invalid or missing");
	}
});


router.get('/object/:mykey', function(req, res, next) {
	
	var key = req.params.mykey;
	var database = new DB;

	database.connect(mongoDbUri)
	.then(
		function() {
			if(req.query.timestamp){
				//console.log("Timestamp: " + req.query.timestamp);
				return database.getValueByTime(config.client.mongodb.defaultCollection,
				key, new Date(req.query.timestamp*1000));
			}
			else{
				//console.log("NO timestamp");
				return database.getLatestValue(config.client.mongodb.defaultCollection,
				key);
			}
		})
	.then(
		function(value) {
			return {
					"success": true,
					"value": value,
					"error": ""
				};
		},
		function(err) {
			console.log("Failed to retrieve value: " + err);
			return {
					"success": false,
					"value": "",
					"error": "Failed to retrieve value: " + err
				};
		})
	.then(
		function(resultObject) {
			database.close();
			res.json(resultObject);
		})
});



router.get('/connstr', function(req, res, next) {
	res.send(mongoDbUri);
});




module.exports = router;