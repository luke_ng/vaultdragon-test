var config = {
    expressPort: 3000,
    client: {
        mongodb: {
            defaultDatabase: "vaultd",
            defaultCollection: "objects",
            defaultUri: "mongodb://localhost:27017",
            opts: "authSource=admin&socketTimeoutMS=30000&maxPoolSize=20"
        }
    }
};

module.exports = config;